from pyspark.sql import SparkSession
from pyspark.sql.functions import desc, row_number
from pyspark.sql.window import Window
from utils import (names_csv_file,
                   titles_csv_file,
                   ratings_csv_file,
                   principals_csv_file)
from utils import read_from_csv, write_to_csv

spark = SparkSession.builder.master('local').appName('task_5').getOrCreate()

namesDf = read_from_csv(names_csv_file, spark)
titlesDf = read_from_csv(titles_csv_file, spark)
ratingsDf = read_from_csv(ratings_csv_file, spark)
principalsDf = read_from_csv(principals_csv_file, spark)

persNamesDf = namesDf.select('nconst', 'primaryName')
moviesDf = (titlesDf
            .select('tconst', 'primaryTitle', 'startYear')
            .where(titlesDf.titleType == 'movie'))
ratedMoviesDf = moviesDf.join(ratingsDf, 'tconst', 'inner')
directorsDf = (principalsDf
               .select('tconst', 'nconst')
               .where(principalsDf.category == 'director'))

dirRankedDf = ((directorsDf
                .join(ratedMoviesDf, 'tconst', 'inner')
                .withColumn('rank', row_number()
                            .over(Window
                                  .partitionBy('nconst')
                                  .orderBy(desc('averageRating'),
                                           desc('numVotes')))))
               .select('tconst',
                       'nconst',
                       'primaryTitle',
                       'startYear',
                       'averageRating',
                       'numVotes',
                       'rank'))


topTitlesByDirector = ((dirRankedDf
                        .join(persNamesDf, 'nconst', 'inner'))
                       .select('primaryName',
                               'primaryTitle',
                               'startYear',
                               'averageRating',
                               'numVotes')
                       .where(dirRankedDf.rank < 6)
                       .orderBy('nconst',
                                desc('averageRating'),
                                desc('numVotes')))

write_to_csv(topTitlesByDirector, 'output_top_titles_by_director')
