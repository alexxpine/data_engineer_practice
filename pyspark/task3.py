from pyspark.sql import SparkSession
from pyspark.sql import types as t
from pyspark.sql.functions import desc, split, explode, row_number, lit
from pyspark.sql.window import Window
from datetime import date
from utils import titles_csv_file, ratings_csv_file
from utils import read_from_csv, write_to_csv

spark = SparkSession.builder.master('local').appName('task_3').getOrCreate()

titlesDf = read_from_csv(titles_csv_file, spark)
ratingsDf = read_from_csv(ratings_csv_file, spark)

moviesDf = (titlesDf
            .select('tconst', 'primaryTitle', 'startYear', 'genres')
            .where((titlesDf.titleType == 'movie')
                   & (titlesDf.startYear > 1949)))
splitGenresDf = (moviesDf
                 .withColumn('genre', explode(split('genres', '[,]'))))

popTitlesDf = ratingsDf.select('*').where(ratingsDf.numVotes > 100000)
popByGenresDf = splitGenresDf.join(popTitlesDf, 'tconst', 'inner')


def top_titles_by_years(start, end):

    filterByYears = ((popByGenresDf
                      .withColumn('yearRange', lit(f'{start}-{end}')))
                     .select('*')
                     .where(popByGenresDf.startYear.between(start, end)))

    ranked = (filterByYears
              .withColumn('rank', row_number()
                          .over(Window
                                .partitionBy('genre')
                                .orderBy(desc('averageRating'))))
              .select('*')
              .orderBy('genre', desc('averageRating'), desc('numVotes')))

    return (ranked
            .select('tconst',
                    'primaryTitle',
                    'startYear',
                    'genre',
                    'averageRating',
                    'numVotes',
                    'yearRange')
            .where(ranked.rank <= 10))


topTitlesByTenYears = spark.sparkContext.emptyRDD()
schema = t.StructType([t.StructField('tconst', t.StringType(), True),
                       t.StructField('primaryTitle', t.StringType(), True),
                       t.StructField('startYear', t.StringType(), True),
                       t.StructField('genres', t.StringType(), True),
                       t.StructField('averageRating', t.DoubleType(), True),
                       t.StructField('numVotes', t.IntegerType(), True),
                       t.StructField('yearRange', t.StringType(), True)])
df = spark.createDataFrame(data=topTitlesByTenYears, schema=schema)

topTitlesByTenYears = df

for year in range(1950, date.today().year, 10):
    topTitlesByTenYears = (topTitlesByTenYears
                           .union(top_titles_by_years(year, year + 9)))

write_to_csv(topTitlesByTenYears, 'output_top_titles_by_ten_years')
