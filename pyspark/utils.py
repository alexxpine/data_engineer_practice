titles_csv_file = 'data\\title.basics.tsv\\data.tsv'
ratings_csv_file = ('data\\title.ratings.tsv\\data.tsv')
names_csv_file = 'data\\name.basics.tsv\\data.tsv'
principals_csv_file = 'data\\title.principals.tsv\\data.tsv'


def read_from_csv(file_path, spark):
    return (spark
            .read
            .option('sep', '\t')
            .option('header', 'true')
            .option('inferSchema', 'true')
            .csv(file_path))


def write_to_csv(df, folder_name):
    return (df
            .coalesce(1)
            .write
            .option('header', 'true')
            .csv(folder_name))
