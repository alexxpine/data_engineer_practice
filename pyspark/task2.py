from pyspark.sql import SparkSession
from pyspark.sql.functions import desc, split, explode, row_number
from pyspark.sql.window import Window
from utils import titles_csv_file, ratings_csv_file
from utils import read_from_csv, write_to_csv

spark = SparkSession.builder.master('local').appName('task_2').getOrCreate()

titlesDf = read_from_csv(titles_csv_file, spark)
ratingsDf = read_from_csv(ratings_csv_file, spark)

moviesDf = (titlesDf
            .select('tconst', 'primaryTitle', 'startYear', 'genres')
            .where(titlesDf.titleType == 'movie'))
moviesSplitGenresDf = (moviesDf
                       .withColumn('genres', explode(split('genres', '[,]'))))

popTitlesDf = ratingsDf.select('*').where(ratingsDf.numVotes > 100000)
topByGenresDf = (moviesSplitGenresDf
                 .join(popTitlesDf, 'tconst', 'inner')
                 .withColumn('rank', row_number()
                             .over(Window.partitionBy('genres')
                             .orderBy(desc('averageRating'),desc('numVotes'))))
                 .orderBy('genres', desc('averageRating'), desc('numVotes')))
OutputTopDf = (topByGenresDf
               .select('tconst',
                       'primaryTitle',
                       'startYear',
                       'genres',
                       'averageRating',
                       'numVotes')
               .where(topByGenresDf.rank <= 10))

write_to_csv(OutputTopDf, 'output_top_titles_by_genres')
