from pyspark.sql import SparkSession
from pyspark.sql.functions import count, desc
from pyspark.sql.window import Window
from utils import (names_csv_file,
                   titles_csv_file,
                   ratings_csv_file,
                   principals_csv_file)
from utils import read_from_csv, write_to_csv

spark = SparkSession.builder.master('local').appName('task_4').getOrCreate()

namesDf = read_from_csv(names_csv_file, spark)
titlesDf = read_from_csv(titles_csv_file, spark)
ratingsDf = read_from_csv(ratings_csv_file, spark)
principalsDf = read_from_csv(principals_csv_file, spark)

popTitlesDf = (ratingsDf
               .select('tconst', 'averageRating', 'numVotes')
               .where(ratingsDf.numVotes > 100000))
moviesDf = titlesDf.select('tconst').where(titlesDf.titleType == 'movie')
topTitlesDf = (moviesDf
               .join(popTitlesDf, 'tconst', 'inner')
               .orderBy(desc('averageRating'), desc('numVotes'))
               .limit(100))

popActorsDf = ((topTitlesDf
                .join(principalsDf, 'tconst', 'inner')
                .where(principalsDf.category == 'actor'))
               .select('tconst', 'nconst'))
countActorsDf = (popActorsDf
                 .withColumn('countTitles',
                             count('*').over(Window.partitionBy('nconst'))))
topActorsDf = ((countActorsDf
                .select('nconst')
                .where(countActorsDf.countTitles > 1)
                .distinct()
                .orderBy(desc('countTitles')))
               .join(namesDf
                     .select('nconst', 'primaryName'), 'nconst', 'inner')
               .select('primaryName'))

write_to_csv(topActorsDf, 'output_top_actors')
