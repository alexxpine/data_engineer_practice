from pyspark.sql import SparkSession
from pyspark.sql.functions import desc
from datetime import date
from utils import read_from_csv, write_to_csv
from utils import titles_csv_file, ratings_csv_file

spark = SparkSession.builder.master('local').appName('task_1').getOrCreate()


def output(df):
    return (df
            .select('tconst',
                    'primaryTitle',
                    'numVotes',
                    'averageRating',
                    'startYear'))


titlesDf = read_from_csv(titles_csv_file, spark)
ratingsDf = read_from_csv(ratings_csv_file, spark)

moviesDf = (titlesDf
            .select('tconst', 'primaryTitle', 'startYear')
            .where(titlesDf.titleType == 'movie'))

popTitlesDf = ratingsDf.select('*').where(ratingsDf.numVotes > 100000)

topTitlesDf = (moviesDf
               .join(popTitlesDf, 'tconst', 'inner')
               .orderBy(desc('averageRating'), desc('numVotes'))
               .limit(100))

topTitles_lastTenDf = (moviesDf
                       .join(popTitlesDf, 'tconst', 'inner')
                       .where(moviesDf.startYear > date.today().year - 10)
                       .orderBy(desc('averageRating'), desc('numVotes'))
                       .limit(100))

topTitles_sixtiesDf = (moviesDf
                       .join(popTitlesDf, 'tconst', 'inner')
                       .where(moviesDf.startYear.between(1960, 1969))
                       .orderBy(desc('averageRating'), desc('numVotes'))
                       .limit(100))

outputTopTitlesDf = output(topTitlesDf)
outputTopTitles_lastTenDf = output(topTitles_lastTenDf)
outputTopTitles_sixtiesDf = output(topTitles_sixtiesDf)

write_to_csv(outputTopTitlesDf, 'output_top_titles')
write_to_csv(outputTopTitles_lastTenDf, 'output_top_titles_last_ten')
write_to_csv(outputTopTitles_sixtiesDf, 'output_top_titles_sixties')
