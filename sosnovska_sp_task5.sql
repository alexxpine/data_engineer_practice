/* 5. Задача: Создать функцию триггера и триггер 
   для оформления заказа и добавления его в таблицы 
   <your_lastname>.salesorderheader и <your_lastname>.salesorderdetail 
   (триггер для этой таблицы с действиями(update,delete,insert)).
   Менеджер добавляет товар в таблицу <your_lastname>.salesorderdetail 
   после добавления каждого нового товара или удалении из таблицы из этой таблицы, 
   общая информация должна вставляться, удаляться или обновляться 
   (обновляется только столбец totaldue = sum(totaline from <your_lastname>.salesorderdetail)) 
   в таблице <your_lastname>.salesorderheader.
   Если значение столбца totaline изменилось или строка была удалена 
   нужно  добавить или отнять это значение в totaldue. 
   При удаления всех данных для одного заказа <your_lastname>.salesorderdetail. 
   нужно удалить все данные в таблице <your_lastname>.salesorderheader для этого заказа.
   Условие:
   Создать скрипт:  <your_lastname>_sp_task5.sql
   Имя процедуры - <your_lastname>.<procedure_name>_task5
   Имя триггера - <your_lastname>_<trigger_name>_task5 */

drop table if exists sosnovskaya.salesorderheader;
create table sosnovskaya.salesorderheader (like sales.salesorderheader including all);

insert into sosnovskaya.salesorderheader
select * 
from sales.salesorderheader;

drop table if exists sosnovskaya.salesorderdetail;
create table sosnovskaya.salesorderdetail (like sales.salesorderdetail including all);

insert into sosnovskaya.salesorderdetail
select * 
from sales.salesorderdetail;

alter table sosnovskaya.salesorderdetail
add column customerid int, 
add column salespersonid int,
add column creditcardid int;

alter table sosnovskaya.salesorderheader
drop column revisionnumber,
drop column duedate,
drop column shipdate,
drop column salesordernumber,
drop column purchaseordernumber,
drop column accountnumber,
drop column territoryid,
drop column billtoaddressid,
drop column shipmethodid,
drop column creditcardapprovalcode,
drop column currencyrateid,
drop column subtotal,
drop column taxamt,
drop column freight,
drop column comment,
drop column rowguid,
drop column modifieddate,
drop column shiptoaddressid;

CREATE OR REPLACE FUNCTION sosnovskaya.set_order_task5() RETURNS TRIGGER AS $set_order$
    DECLARE
        res int;
    BEGIN
        IF (TG_OP = 'DELETE') THEN
            SELECT salesorderid 
              INTO res 
              FROM sosnovskaya.salesorderdetail 
             WHERE OLD.salesorderid = salesorderid;
            IF NOT FOUND THEN
                DELETE FROM sosnovskaya.salesorderheader 
                 WHERE OLD.salesorderid = salesorderid;
            ELSE
                UPDATE sosnovskaya.salesorderheader 
                   SET totaldue = totaldue - OLD.linetotal 
                 WHERE salesorderid = OLD.salesorderid;
            END IF;
        ELSIF (TG_OP = 'UPDATE') THEN
            UPDATE sosnovskaya.salesorderheader SET totaldue = totaldue + (NEW.linetotal - OLD.linetotal) WHERE salesorderid = OLD.salesorderid;
        ELSIF (TG_OP = 'INSERT') THEN
            UPDATE sosnovskaya.salesorderheader SET totaldue = totaldue + NEW.linetotal WHERE salesorderid = NEW.salesorderid;
        END IF;
        RETURN NULL;
    END;
$set_order$ LANGUAGE plpgsql;

CREATE TRIGGER set_order
 AFTER INSERT OR UPDATE OR DELETE ON sosnovskaya.salesorderdetail
   FOR EACH ROW EXECUTE FUNCTION sosnovskaya.set_order_task5();

update sosnovskaya.salesorderdetail set linetotal = 5 where salesorderdetailid = 1;
insert into sosnovskaya.salesorderdetail (salesorderid, salesorderdetailid, linetotal, orderqty, productid, specialofferid, unitprice) values (43659, 50, 5, 1, 700, 12, 5);
delete from sosnovskaya.salesorderdetail where salesorderid = 43660 and salesorderdetailid = 13;
delete from sosnovskaya.salesorderdetail where salesorderid = 43660 and salesorderdetailid = 14;
