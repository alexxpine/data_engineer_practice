/* 1. Создайте таблицу Customer со следующими колонками:
   CustomerID int,
   FirstName varchar(50),
   LastName varchar(50),
   Email varchar(100),
   ModifiedDate date,
   Age int,
   active boolean
   На колонке CustomerID создайте ограничение первичного ключа и индекс типа B-tree 
   Заполните ее тестовыми данными. */

CREATE TABLE IF NOT EXISTS sosnovskaya.customer (
    customer_id int,
    first_name varchar(50),
    last_name varchar(50),
    email varchar(100),
    modified_date date,
    age int,
    active boolean,
    PRIMARY KEY (customer_id)
);

CREATE INDEX customer_idx ON sosnovskaya.customer USING btree (customer_id);

INSERT INTO sosnovskaya.customer
SELECT 
    businessentityid AS customer_id,
    concat('firstname', businessentityid) AS first_name,
    concat('lastname', businessentityid) AS lastname,
    concat('firstname', businessentityid, 'lastname', businessentityid, '@email.com') AS email,
    modifieddate AS modified_date,
    DATE_PART('year', now()::date) - DATE_PART('year', birthdate::date) AS age,
    CASE WHEN businessentityid % 7 = 0 THEN False ELSE True END AS active
  FROM humanresources.employee;

/* 2. Удостоверьтесь, что ваш индекс появился в системном каталоге pg_indexes. */

SELECT * FROM pg_indexes WHERE indexname = 'customer_idx';

/* 3. Создайте составной индекс типа B-tree на таблице Customer на колонках FirstName и LastName. */

CREATE INDEX fullname_idx ON sosnovskaya.customer USING btree (first_name, last_name);

SELECT * FROM pg_indexes WHERE indexname = 'fullname_idx';

/* 4. Создайте такой индекс на таблице Customer, чтобы результат выполнения запроса
   explain (analyze)
   select *
     from student.customer
    where age between 30 and 60
   был:
   Index Scan using ix_customer_age on customer (...)
   Index Cond: ((age >= 30) AND (age <= 60)) */

CREATE INDEX ix_customer_age ON sosnovskaya.customer (age);

EXPLAIN (ANALYZE)
 SELECT age 
   FROM sosnovskaya.customer
  WHERE age BETWEEN 30 AND 60;

/* 5. Создайте покрывающий индекс IX_Customer_ModifiedDate для быстрого выполнения запроса
   и проверьте, что он используется в плане запроса:
   SELECT
     FirstName,
     LastName
    FROM Customer
   WHERE ModifiedDate = ‘2020-10-20’ */
   
CREATE INDEX IX_Customer_ModifiedDate ON sosnovskaya.customer (modified_date) INCLUDE (first_name, last_name);

EXPLAIN (ANALYZE)
 SELECT first_name, last_name
   FROM sosnovskaya.customer
  WHERE modified_date = '2020-10-20';
  
/* 6. Удалите индекс PK_CustomerID из таблицы Customer */

SELECT * FROM pg_indexes WHERE indexname LIKE 'PK_C%';

DROP INDEX customer_idx;

/* 7. Создайте индекс типа Hash с названием PK_Modified_Date 
   в таблице Customer на колонке ModifiedDate */

CREATE INDEX PK_Modified_Date ON sosnovskaya.customer USING hash (modified_date);

/* 8. Переименуйте индекс PK_Modified_Date на PK_ModifiedDate */

ALTER INDEX PK_Modified_Date RENAME TO PK_ModifiedDate;

/* 9. Создайте частичный индекс на колонке email только для тех записей, 
   у которых active = true. 
   И напишите запрос к таблице, в котором этот индекс будет использоваться. */
   
CREATE INDEX email_active_idx ON sosnovskaya.customer (email) WHERE active = True;

EXPLAIN (ANALYZE)
 SELECT first_name, last_name
   FROM sosnovskaya.customer
  WHERE email = 'firstname1lastname1@email.com' AND active = True;
  
/* 10. Создайте функциональный индекс в таблице Customer для быстрого поиска записей 
   по такому правилу: 
   если firstname = ‘firstname1’ и lastname = ‘lastname1’, 
   то мы ищем ‘f, lastname1’.
   Проверьте план запроса, что этот индекс используется. */

CREATE INDEX fullname_func_idx ON sosnovskaya.customer ((substring(first_name, 1, 1) || ', ' || last_name));

EXPLAIN (ANALYZE)
 SELECT *
   FROM sosnovskaya.customer
  WHERE (substring(first_name, 1, 1) || ', ' || last_name) = 'f, lastname259';
