/* Создать представление Person.vPerson, 
   которое базируется на таблицах Person.Person и Person.EmailAddress, 
   и содержит следующие колонки:
   - Title
   - FirstName
   - LastName
   - EmailAddress */
CREATE VIEW sosnovskaya.vPerson AS
    SELECT p.Title, p.FirstName, p.LastName, ea.EmailAddress
      FROM Person.Person AS p
      JOIN Person.EmailAddress as ea
        ON p.BusinessEntityId = ea.BusinessEntityId;

/* Напишите запрос, который вернет колонки:
   - HumanResources.Employee.BusinessEntityId
   - HumanResources.Employee.NationalIdNumber
   - HumanResources.Employee.JobTitle
   - Person.Person.FirstName
   - Person.Person.LastName
   - Person.PersonPhone.PhoneNumber
   Основная таблица - HumanResources.Employee, 
   Person.Person и Person.PersonPhone оформить как CTE */
WITH full_name AS (
        SELECT p.BusinessEntityId, p.FirstName, p.LastName
          FROM Person.Person AS p
     ), phone_number AS (
        SELECT pp.BusinessEntityId, pp.PhoneNumber
          FROM Person.PersonPhone AS pp
     )
SELECT e.BusinessEntityId, e.NationalIdNumber, e.JobTitle, fn.FirstName, fn.LastName, pn.PhoneNumber
  FROM HumanResources.Employee AS e
  JOIN full_name AS fn USING (BusinessEntityId)
  JOIN phone_number AS pn USING (BusinessEntityId);
