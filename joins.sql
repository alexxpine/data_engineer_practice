/* Выберите имя и фамилию, должность (Job Title), дату рождения, 
   используя таблицы [Person].[Person] и [HumanResources].[Employee]. 
   Записи должны иметь соответствие в правой и левой таблице */
SELECT p.FirstName, p.LastName, e.JobTitle, e.BirthDate
  FROM Person.Person AS p
  JOIN HumanResources.Employee AS e
    ON p.BusinessEntityId = e.BusinessEntityId;
	
/* Выберите имя и фамилию, используя таблицы [Person].[Person], 
   и должность (Job Title) подзапросом, используя таблицу [HumanResources].[Employee]. */
SELECT p.FirstName, p.LastName,
       (SELECT e.JobTitle 
          FROM HumanResources.Employee AS e
         WHERE p.BusinessEntityId = e.BusinessEntityId) AS JobTitle
  FROM Person.Person AS p;
  
/* Используя запрос из пункта 1.2 удалите из выборки все записи, 
   для которых JobTitle является NULL (используя вложенные подзапросы) */
SELECT p.FirstName, p.LastName,
       (SELECT e.JobTitle 
          FROM HumanResources.Employee AS e
         WHERE p.BusinessEntityId = e.BusinessEntityId) AS JobTitle
  FROM Person.Person AS p
 WHERE p.BusinessEntityId IN
       (SELECT e.BusinessEntityId 
          FROM HumanResources.Employee AS e);

/* Напишите запрос, который вернет все возможные сочетания 
   имени, фамилии из таблицы [Person].[Person] с должностями 
   из таблицы [HumanResources].[Employee] */
SELECT p.FirstName, p.LastName, e.JobTitle
  FROM Person.Person AS p
 CROSS JOIN HumanResources.Employee AS e;
 
/* Используя функцию COUNT() напишите запрос, 
   который выведет количество записей из запроса 1.4 */
SELECT COUNT(*)
  FROM Person.Person AS p
 CROSS JOIN HumanResources.Employee AS e;