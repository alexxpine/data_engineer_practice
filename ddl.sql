CREATE TABLE IF NOT EXISTS sosnovskaya.testtable (
	id          INT  NOT NULL PRIMARY KEY,
	name        TEXT,
	issold      BIT,
    invoicedate DATE
);

INSERT INTO sosnovskaya.TestTable
VALUES (1, 'Boat', B'1', '2021-11-08'),
       (2, 'Auto', B'0', '2021-11-09'),
       (3, 'Plane', null, '2021-12-09');

ALTER TABLE sosnovskaya.TestTable RENAME name TO vehicle; 

SELECT * FROM sosnovskaya.TestTable;

TRUNCATE sosnovskaya.TestTable;

DROP TABLE IF EXISTS sosnovskaya.TestTable;