/* Задача: Создать хранимую процедуру для получения отчетов, которые записываются в таблицы.
   ежедневные -  <your_lastname>.sales_report_total_daily 
   месячные -  <your_lastname>.sales_report_total_monthly 
   годовые -  <your_lastname>.sales_report_total_yearly
   Используйте таблицу sales.salesorderheader
   Параметры:
   <inp> – int. */
   
drop table if exists sosnovskaya.sales_report_total_daily;
create table sosnovskaya.sales_report_total_daily (
    date_report         timestamp with time zone,
    onlineorderflag     boolean,
    sum_total           numeric,
    avg_total           numeric,
    qty_orders          int);
drop table if exists sosnovskaya.sales_report_total_monthly;
create table if not exists sosnovskaya.sales_report_total_monthly (
    date_report         timestamp with time zone,
    onlineorderflag     boolean,
    sum_total           numeric,
    avg_total           numeric,
    qty_orders          int);
drop table if exists sosnovskaya.sales_report_total_yearly;
create table if not exists sosnovskaya.sales_report_total_yearly (
    date_report         timestamp with time zone,
    onlineorderflag     boolean,
    sum_total           numeric,
    avg_total           numeric,
    qty_orders          int);


CREATE OR REPLACE PROCEDURE sosnovskaya.sales_report_task3(
    IN input_data integer
) AS 
$$
    BEGIN
        INSERT INTO sosnovskaya.sales_report_total_daily
        WITH daily_total_cte AS (
              SELECT orderdate, 
                     onlineorderflag, 
                     SUM(subtotal) AS sum_total, 
                     AVG(subtotal) AS avg_total, 
                     COUNT(*) AS qty_orders
                FROM sales.salesorderheader
               WHERE date_part('year', current_timestamp) - date_part('year', orderdate) < input_data
               GROUP BY orderdate, onlineorderflag
               ORDER BY orderdate, onlineorderflag)
        SELECT *
          FROM daily_total_cte;
		  
        INSERT INTO sosnovskaya.sales_report_total_monthly
        WITH monthly_total_cte AS (
              SELECT date_trunc('month', orderdate), 
                     onlineorderflag, 
                     SUM(subtotal) AS sum_total, 
                     AVG(subtotal) AS avg_total, 
                     COUNT(*) AS qty_orders
                FROM sales.salesorderheader
               WHERE date_part('year', current_timestamp) - date_part('year', orderdate) < input_data
               GROUP BY date_trunc('month', orderdate), onlineorderflag
               ORDER BY date_trunc('month', orderdate), onlineorderflag)
        SELECT *
          FROM monthly_total_cte;

        INSERT INTO sosnovskaya.sales_report_total_yearly
        WITH yearly_total_cte AS (
              SELECT date_trunc('year', orderdate), 
                     onlineorderflag, 
                     SUM(subtotal) AS sum_total, 
                     AVG(subtotal) AS avg_total, 
                     COUNT(*) AS qty_orders
                FROM sales.salesorderheader
               WHERE date_part('year', current_timestamp) - date_part('year', orderdate) < input_data
               GROUP BY date_trunc('year', orderdate), onlineorderflag
               ORDER BY date_trunc('year', orderdate), onlineorderflag)
        SELECT *
          FROM yearly_total_cte;
    END;
$$
LANGUAGE plpgsql;

CALL sosnovskaya.sales_report_task3(11);
SELECT * FROM sosnovskaya.sales_report_total_daily;
SELECT * FROM sosnovskaya.sales_report_total_monthly;
SELECT * FROM sosnovskaya.sales_report_total_yearly;
