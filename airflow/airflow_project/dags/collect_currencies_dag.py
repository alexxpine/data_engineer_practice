from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from spark_jobs.collect_currencies_job import collect_currencies, services
from spark_jobs.utils import current_date

start_date = current_date.replace(second=0, microsecond=0)

default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": datetime(2022, 5, 26),
    "email": ["my_email@mail.com"],
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=1)
}


with DAG("collect_currencies_dag",
         default_args=default_args,
         schedule_interval='0/1 * * * *',
         catchup=False) as dag:
    for service in services:
        collect_data = PythonOperator(
            task_id=f'data_for_{service}',
            python_callable=collect_currencies,
            op_kwargs={'name': service}
        )

        collect_data
