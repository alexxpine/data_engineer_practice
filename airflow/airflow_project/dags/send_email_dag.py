from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from spark_jobs.send_email_job import send_statistics_by_email
from spark_jobs.utils import current_date

start_date = current_date.replace(second=0, microsecond=0)

default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": datetime(2022, 5, 26),
    "email": ["my_email@mail.com"],
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=1)
}


with DAG("send_statistics_by_email_dag",
         default_args=default_args,
         schedule_interval='2/6 * * * *',
         catchup=False) as dag:
    send_statistics = PythonOperator(
        task_id='send_statistics',
        python_callable=send_statistics_by_email
    )

    send_statistics
