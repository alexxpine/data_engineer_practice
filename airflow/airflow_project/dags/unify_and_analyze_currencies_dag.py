from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from spark_jobs.unify_currencies_job import unify_raw_data, exchange_data
from spark_jobs.aggregate_currencies_job import aggregate_currencies
from spark_jobs.utils import current_date

start_date = current_date.replace(second=0, microsecond=0)

default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": datetime(2022, 5, 26),
    "email": ["my_email@mail.com"],
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=1)
}


with DAG("unify_and_analyze_currencies_dag",
         default_args=default_args,
         schedule_interval='0/3 * * * *',
         catchup=False) as dag:
    unify_data = PythonOperator(
        task_id='unify_data',
        python_callable=unify_raw_data,
        op_kwargs={'exchange_data': exchange_data}
    )
    analyze_data = PythonOperator(
        task_id='analyze_data',
        python_callable=aggregate_currencies
    )

    unify_data >> analyze_data
