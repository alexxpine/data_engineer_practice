import io
import pandas as pd
import requests
from minio.error import MinioException
from spark_jobs.utils import services, client


class RetrievedData:
    def __init__(self, name):
        self._url = services.get(name)
        self._name = name

    def receive_data(self):
        response = requests.get(self._url).json()
        df = pd.DataFrame(response)
        return df

    def write_to_csv(self, df):
        file_name = f'/tmp/data/{self._name}.csv'
        with open(file_name, 'w') as file:
            df.to_csv(file, index=False, header=True)

    def upload_download_csv(self, df):
        file_name = f'{self._name}.csv'
        bucket_name = 'minutelystorage'
        try:
            minio_response = client.get_object(bucket_name, file_name)
            extracted_df = pd.read_csv(minio_response, dtype=str)
            diff_df = self._compare_df(df.astype('str'), extracted_df)
            updated_df = pd.concat([extracted_df, diff_df], ignore_index=True)
            self._put_to_minio(client, bucket_name, file_name, updated_df)
            return updated_df
        except MinioException as err:
            self._put_to_minio(client, bucket_name, file_name, df)
            print(err)
            return df

    def _compare_df(self, df_api, df_minio):
        return df_api[~df_api.apply(tuple, 1).isin(df_minio.apply(tuple, 1))]

    def _put_to_minio(self, client, bucket_name, file_name, df):
        csv_bytes = df.to_csv(index=False).encode('utf-8')
        csv_buffer = io.BytesIO(csv_bytes)
        client.put_object(
            bucket_name,
            file_name,
            csv_buffer,
            length=len(csv_bytes),
            content_type='application/csv'
        )


def collect_currencies(name, **kwargs):
    exchange = RetrievedData(name)
    data = exchange.receive_data()
    exchange.upload_download_csv(data)
