from minio import Minio
import pandas as pd
from datetime import datetime
from sqlalchemy import create_engine

services = {
  'bitfinex': 'https://api.bitfinex.com/v1/trades/btcusd?limit_trades=500',
  'bitmex': 'https://www.bitmex.com/api/v1/trade?symbol=XBTUSD&count=500&reverse=true',
  'poloniex': 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=USDT_BTC',
}

exchange_data = pd.DataFrame({'timestamp': pd.Series(dtype='str'),
                              'exchange': pd.Series(dtype='str'),
                              'type': pd.Series(dtype='str'),
                              'amount': pd.Series(dtype='str'),
                              'price': pd.Series(dtype='str')})

current_date = datetime.now()

client = Minio('s3:9000',
               'minio_access_key',
               'minio_secret_key',
               secure=False)

postgres_connection = 'postgresql://airflow:airflow@database:5432/airflow'
engine = create_engine(postgres_connection)
