import io
import time
import numpy as np
import pandas as pd
import sqlalchemy.types as st
from sqlalchemy.orm import sessionmaker
from datetime import datetime, timedelta, timezone
from minio.error import MinioException
from minio.commonconfig import CopySource
from spark_jobs.utils import client, engine


def read_from_parquet():
    file_name_to_read = 'exchange_data.parquet'
    file_name_to_write = f'exchange_data_{time.time()}.parquet'
    bucket_name = 'hourlystorage'
    try:
        minio_response = client.get_object(bucket_name, file_name_to_read)
        client.copy_object(bucket_name, file_name_to_write, CopySource(bucket_name, file_name_to_read))
        client.remove_object(bucket_name, file_name_to_read)
        extracted_data = minio_response.data
        df = pd.read_parquet(io.BytesIO(extracted_data))
        return df
    except MinioException as err:
        print(err)
        return None


def download_to_postgres(df):
    table_name = 'currencies'
    if df is not None:
        df['timestamp'] = pd.to_datetime(pd.to_numeric(df['timestamp'], errors='coerce').fillna(0).astype(np.int64), unit='s')
        df.to_sql(table_name,
                  engine,
                  index=False,
                  if_exists='append',
                  dtype={'timestamp': st.TIMESTAMP,
                         'exchange': st.String,
                         'type': st.String,
                         'amount': st.Float,
                         'price': st.Float})


def analytic_query():
    end_date = (datetime
                .now(timezone.utc)
                .replace(second=0, microsecond=0))  # minute=0, second=0, microsecond=0
    start_date = end_date - timedelta(minutes=3)  # timedelta(hours=1)
    query_select = f'''WITH sell_cte AS (
                            SELECT c.type,
                                COUNT(*) AS count_transactions_sell,
                                SUM(c.amount * c.price) AS total_sum_sell,
                                AVG(c.price) AS average_rate_sell
                            FROM currencies AS c
                            WHERE c.type = 'sell' AND c.timestamp BETWEEN '{start_date}' AND '{end_date}'
                            GROUP BY c.type
                        ), buy_cte AS (
                            SELECT c.type,
                                COUNT(*) AS count_transactions_buy,
                                SUM(c.amount * c.price) AS total_sum_buy,
                                AVG(c.price) AS average_rate_buy
                            FROM currencies AS c
                            WHERE c.type = 'buy' AND c.timestamp BETWEEN '{start_date}' AND '{end_date}'
                            GROUP BY c.type
                        )

                    SELECT '{start_date}' AS start_date,
                        '{end_date}' AS end_date,
                        sc.count_transactions_sell,
                        sc.total_sum_sell,
                        sc.average_rate_sell,
                        bc.count_transactions_buy,
                        bc.total_sum_buy,
                        bc.average_rate_buy
                     FROM sell_cte AS sc
                    CROSS JOIN buy_cte AS bc'''

    query_delete = f'''DELETE FROM currencies AS c
                       WHERE c.timestamp < '{end_date}' '''

    return query_select, query_delete


def insert_result(query_select, query_delete):
    table_name = 'results'

    Session = sessionmaker(bind=engine)
    session = Session()
    session.execute(f'''CREATE TABLE IF NOT EXISTS {table_name} (start_date text,
                                        end_date text,
                                        count_transactions_sell int,
                                        total_sum_sell double precision,
                                        average_rate_sell double precision,
                                        count_transactions_buy int,
                                        total_sum_buy double precision,
                                        average_rate_buy double precision)''')
    session.execute(f'INSERT INTO {table_name} ({query_select})')
    session.execute(query_delete)
    session.commit()
    session.close()


def aggregate_currencies(**kwargs):
    df = read_from_parquet()
    download_to_postgres(df)
    query_select, query_delete = analytic_query()
    insert_result(query_select, query_delete)
