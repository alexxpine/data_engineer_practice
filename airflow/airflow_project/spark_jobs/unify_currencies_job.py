import io
import pandas as pd
from minio.error import MinioException
from spark_jobs.utils import exchange_data, client


class CryptoProcessing:
    def __init__(self, name):
        self._name = name

    def download_from_minio(self):
        file_name = f'{self._name}.csv'
        bucket_name = 'minutelystorage'
        try:
            minio_response = client.get_object(bucket_name, file_name)
            df = pd.read_csv(minio_response)
            return df
        except MinioException as err:
            print(err)
            return None

    def remove_from_minio(self):
        file_name = f'{self._name}.csv'
        bucket_name = 'minutelystorage'
        try:
            client.remove_object(bucket_name, file_name)
        except MinioException as err:
            print(err)

    def unifyRawDf(self, df):
        if df is not None:
            df['exchange'] = self._name
            df = self.modifyDf(df)
            return df[['timestamp', 'exchange', 'type', 'amount', 'price']]

    def modifyDf(self, df):
        return df


class BitfinexProcessing(CryptoProcessing):
    def modifyDf(self, df):
        return df.astype('str')


class BitmexProcessing(CryptoProcessing):
    def modifyDf(self, df):
        df['timestamp'] = pd.to_datetime(df['timestamp'], format='%Y-%m-%dT%H:%M:%S.%fZ').map(pd.Timestamp.timestamp).astype('int')
        df['side'] = df['side'].str.lower()
        df = df.rename(columns={'side': 'type', 'homeNotional': 'amount'})
        return df.astype('str')


class PoloniexProcessing(CryptoProcessing):
    def modifyDf(self, df):
        df['date'] = pd.to_datetime(df['date'], format='%Y-%m-%d %H:%M:%S').map(pd.Timestamp.timestamp).astype('int')
        df = df.rename(columns={'date': 'timestamp', 'rate': 'price'})
        return df.astype('str')


def write_to_parquet(df):
    file_name = 'exchange_data.parquet'
    bucket_name = 'hourlystorage'
    data = io.BytesIO()
    df.to_parquet(data)
    length = data.tell()
    data.seek(0)
    client.put_object(bucket_name, file_name, data, length)


def unify_raw_data(**kwargs):
    exchange_list = [BitfinexProcessing('bitfinex'),
                     BitmexProcessing('bitmex'),
                     PoloniexProcessing('poloniex')]
    unified_data = exchange_data
    for exchange in exchange_list:
        df = exchange.download_from_minio()
        exchange.remove_from_minio()
        unified_df = exchange.unifyRawDf(df)
        unified_data = pd.concat([unified_data, unified_df])
    write_to_parquet(unified_data)
