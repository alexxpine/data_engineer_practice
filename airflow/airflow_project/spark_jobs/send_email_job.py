import pandas as pd
import smtplib, ssl
from datetime import datetime, timedelta, timezone
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from spark_jobs.utils import engine

email_props = {'port': 465,
               'smtp_server': 'smtp.gmail.com',
               'sender': 'test.sosnovska@gmail.com',
               'password': 'TestSosnovska',
               'receiver': ['test.sosnovska@gmail.com'],
               'subject': 'Your daily statistics of cryptocurrency exchanges'}


def retrieve_analytics_df():
    table_name = 'results'
    end_date = (datetime
                .now(timezone.utc)
                .replace(second=0, microsecond=0))  #minute=0
    start_date = end_date - timedelta(minutes=6)  # days=1
    daily_query = f'''SELECT * FROM {table_name} AS t
                       WHERE t.start_date >= '{start_date}' 
                         AND t.end_date <= '{end_date}' '''
    results_df = pd.read_sql(daily_query, engine)
    return results_df


def send_email_basic(df):
    html = """\
        <html>
        <head></head>
        <body>
            {0}
        </body>
        </html>
        """.format(df.to_html(index=False))
    message = MIMEMultipart("multipart")
    part2 = MIMEText(html, "html")
    message.attach(part2)
    message["Subject"] = email_props['subject']
    message["From"] = email_props['sender']
    for receiver in email_props['receiver']:
        message["To"] = receiver

    context = ssl.create_default_context()

    with smtplib.SMTP_SSL(email_props['smtp_server'], email_props['port'], context=context) as server:
        server.login(email_props['sender'], email_props['password'])
        server.sendmail(email_props['sender'], email_props['receiver'], message.as_string())


def send_statistics_by_email():
    df = retrieve_analytics_df()
    send_email_basic(df)
