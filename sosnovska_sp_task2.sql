/* 2. Задача: Создать хранимую процедуру 
   для обновления столбца make_flag в таблице <your_lastname>.product,
   по столбцу name. 
   Замечание: Если для заданного продукта значение флага совпадает 
   вывести на экран  замечание “<YOUR_COMMENT_1>” 
   и если такого продукта нет в таблице вывести “<YOUR_COMMENT_2>”. 
   Параметры:
   <inp_1> - character varying;
   <inp_2> - boolean (true or false). */ 

drop table if exists sosnovskaya.product;
create table sosnovskaya.product as
select *
  from production.product;

CREATE OR REPLACE PROCEDURE sosnovskaya.update_make_flag_task2(
    IN prod_name varchar, flag_value boolean
) AS 
$$
    DECLARE
        res boolean;
    BEGIN
        SELECT makeflag INTO res
          FROM sosnovskaya.product
         WHERE name = prod_name;
	  
        IF NOT FOUND THEN
            RAISE WARNING 'The product % could not be found', prod_name;
        ELSIF res = flag_value THEN
            RAISE WARNING 'The value is already set to %', flag_value;
        ELSE
            UPDATE sosnovskaya.product
               SET makeflag = flag_value
             WHERE name = prod_name;
        END IF;
    END;
$$
LANGUAGE plpgsql;

CALL sosnovskaya.update_make_flag_task2('Bearing Ball', True);
CALL sosnovskaya.update_make_flag_task2('sdasxsx', True);

select * from sosnovskaya.product where name = 'Bearing Ball';
SELECT name AS product_name, makeflag
FROM sosnovskaya.product;