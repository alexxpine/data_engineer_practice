/* Выбрать названия и количество групп из таблицы HumanResources.Department */
SELECT d.GroupName, COUNT(*) AS GroupCount
  FROM HumanResources.Department AS d
 GROUP BY d.GroupName;
 
/* Найти максимальную ставку для каждого сотрудника 
   из таблиц HumanResources.EmployeePayHistory, HumanResources.Employee */
SELECT e.BusinessEntityid, e.JobTitle, MAX(eph.Rate) AS MaxRate
  FROM HumanResources.Employee AS e
  JOIN HumanResources.EmployeePayHistory AS eph
    ON e.BusinessEntityId = eph.BusinessEntityId
 GROUP BY e.BusinessEntityId
 ORDER BY BusinessEntityId;

/* Выбрать минимальную цену единицы товара по подкатегориям 
   (названия из таблицы PRODUCTION.PRODUCTSUBCATEGORY, 
   минимальная цена из таблицы SALES.SALESORDERDETAIL) 
   используя таблицы
   Sales.SalesOrderDetail, 
   Production.Product, 
   Production.ProductSubcategory */
SELECT ps.Name, MIN(sod.UnitPrice) AS MinPrice
  FROM Production.ProductSubcategory AS ps
  JOIN Production.Product AS p
    ON ps.ProductSubcategoryId = p.ProductSubcategoryId
  JOIN Sales.SalesOrderDetail AS sod
    ON p.ProductId = sod.ProductId
 GROUP BY ps.Name;
 
/* Вычислить название и количество подкатегорий товара в каждой категории, 
   используя таблицы 
   Production.ProductCategory, 
   Production.ProductSubcategory */
SELECT pc.Name, COUNT(ps.Name) AS SubcategoryQuantity
  FROM Production.ProductCategory AS pc
  JOIN Production.ProductSubcategory AS ps
    ON pc.ProductCategoryId = ps.ProductCategoryId
 GROUP BY pc.Name;
 
/* Вывести среднюю сумму заказа по подкатегориям товаров, используя таблицы 
   Sales.SalesOrderDetail, 
   Production.Product, 
   Production.ProductSubcategory */
SELECT ps.Name AS subcategory_name, AVG(sod.LineTotal) AS average_sum
  FROM Sales.SalesOrderDetail AS sod
  JOIN Production.Product AS p
    ON sod.ProductId = p.ProductId
  JOIN Production.ProductSubcategory AS ps
    ON p.ProductSubcategoryId = ps.ProductSubcategoryId
 GROUP BY ps.Name;
 
/* Найти ID сотрудника с максимальным рейтом и дату назначения рейта, 
   используя таблицы HumanResources.EmployeePayHistory */
SELECT BusinessEntityId, RateChangeDate, Rate AS MaxRate
  FROM HumanResources.EmployeePayHistory
 WHERE Rate = (
       SELECT MAX(Rate)
         FROM HumanResources.EmployeePayHistory);
  