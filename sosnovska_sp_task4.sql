/* 4. Задача: Создать пользовательскую функцию для определения лучших сотрудников 
   продавшего товаров на большую сумму за отчетный период.
   Учитываются только оффлайн заказы.
   Используйте таблицы sales.salesorderheader, person.person
   Параметры:
   <inp_1> – date;
   <inp_2> – date;
   Возврат функции:
   employeeid(salespesonid, businessentityid) – int;
   firstname – nvarchar(50);
   lastname – nvarchar(50);
   rank – int. */

CREATE OR REPLACE FUNCTION sosnovskaya.top_sales_person_task4(IN date_start date, date_end date)
    RETURNS TABLE (employeeid int, firstname varchar(50), lastname varchar(50), rank bigint)
AS 
$$
    SELECT p.businessentityid AS employeeid, 
           p.firstname, 
           p.lastname, 
		   rank() OVER (ORDER BY SUM(soh.subtotal) DESC)
      FROM person.person AS p
     INNER JOIN sales.salesorderheader AS soh
        ON p.businessentityid = soh.salespersonid
     WHERE soh.onlineorderflag = False 
       AND soh.orderdate BETWEEN date_start AND date_end
     GROUP BY p.businessentityid, p.firstname, p.lastname;
$$
LANGUAGE sql;

SELECT * FROM sosnovskaya.top_sales_person_task4('2011-05-31', '2011-06-30');

DROP FUNCTION IF EXISTS sosnovskaya.top_sales_person_task4;