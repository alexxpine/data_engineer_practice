/* Пересоздать таблицу TestTable скриптом из “ddl.sql”. */

/* Создать скрипт “dml.sql”. В скрипте необходимо прописать команду, 
которая добавит значения ниже в нашу таблицу TestTable */
INSERT INTO testTable (id, name, isSold, invoiceDate)
VALUES (4, 'Bicycle', B'0', '2020-08-23'),
       (5, 'Rocket', B'1', '2020-01-01'),
       (6, 'Motorcycle', null, '2020-08-26'),
       (7, 'Submarine', B'0', '1999-05-16');
	   
/* Дополнить скрипт, добавив команды, которая вставят значения ниже: */
INSERT INTO testTable (id, invoiceDate)
VALUES (8, '2020-08-25');

INSERT INTO testTable (id, name)
VALUES (9, 'Scooter');

/* Дополнить скрипт, написав команду, которая в колонке IsSold поменяет все NULL на 0. */
UPDATE testTable SET isSold = B'0' WHERE isSold IS NULL; 

/* Дополнить скрипт, написав команду, которая удалит все строки, 
   в которых значение колонки Name или InvoiceDate - NULL */
DELETE FROM testTable WHERE name IS NULL OR invoiceDate IS NULL;

/* Используя INSERT, выполнить UPSERT, что означает UPDATE или INSERT. 
   Это позволит вставить строку если ее нет, или обновить существующую. 
   Необходимо этим способом заменить Name Bicycle на Train. 
   Добавить результат в скрипт. */
INSERT INTO testTable (id, name, isSold, invoiceDate)
VALUES (4, 'Train', B'0', '2020-08-23')
ON CONFLICT (id) 
DO 
   UPDATE SET name = EXCLUDED.name;
   
SELECT * FROM testTable;
