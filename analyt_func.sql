/* 1. Найти сумму продаж за месяц по каждому продукту, 
   проданному в январе-2013 года. 
   Вывести итоговый список продуктов без первых и последних 10% списка, 
   используя следующие таблицы:
    Sales.SalesOrderHeader
    Sales.SalesOrderDetail
    Production.Product */
WITH sum_monthly_sales_by_product AS (
       SELECT p.name, SUM(sod.linetotal) AS sumtotal, 
              PERCENT_RANK() OVER (
                  ORDER BY SUM(sod.linetotal)) AS perc_total
         FROM production.product AS p
         JOIN sales.salesorderdetail AS sod
           ON p.productid = sod.productid
         JOIN sales.salesorderheader AS soh
           ON sod.salesorderid = soh.salesorderid
        WHERE date_part('month', soh.orderdate) = 01 
          AND date_part('year', soh.orderdate) = 2013
        GROUP BY p.name
     )
SELECT name, sumtotal
  FROM sum_monthly_sales_by_product
 WHERE perc_total BETWEEN 0.1 AND 0.9;
 
/* 2. Найти самые дешевые продукты в каждой субкатегории продуктов.
   Использовать таблицу Production.Product. */
WITH rank_product_price_by_subcategory AS (
       SELECT name, listprice, productsubcategoryid, 
              DENSE_RANK() OVER (
                  PARTITION BY productsubcategoryid
                      ORDER BY listprice) AS price_rank
         FROM production.product
        ORDER BY productsubcategoryid
   )
SELECT name, listprice
  FROM rank_product_price_by_subcategory
 WHERE price_rank = 1;
 
/* 3. Найти вторую по величине цену для горных велосипедов, 
   используя таблицу Production.Product */
WITH mountain_bike_price AS (
       SELECT listprice, DENSE_RANK() OVER (
              ORDER BY listprice DESC) AS price_rank
         FROM production.product
        WHERE productsubcategoryid = 1
   )
SELECT DISTINCT listprice
  FROM mountain_bike_price
 WHERE price_rank = 2;

/* 4. Посчитать продажи за 2013 год в разрезе категорий(“YoY метрика”):
   (продажи - продажи за прошлый год) / продажи 
   используя таблицы:
    Sales.SalesOrderHeader
    Sales.SalesOrderDetail
    Production.Product
    Production.ProductSubcategory
    Production.ProductCategory */
WITH sum_yearly_sales_by_category AS (
       SELECT pc.name, 
              SUM(sod.linetotal) OVER (
                  PARTITION BY ps.productcategoryid, date_part('year', soh.orderdate)) AS sales, 
              soh.orderdate, 
              pc.productcategoryid
         FROM Sales.SalesOrderHeader AS soh
         JOIN Sales.SalesOrderDetail AS sod
           ON soh.salesorderid = sod.salesorderid
         JOIN Production.Product AS p
           ON sod.productid = p.productid
         JOIN Production.ProductSubcategory AS ps
           ON p.productsubcategoryid = ps.productsubcategoryid
         JOIN Production.ProductCategory AS pc
           ON ps.productcategoryid = pc.productcategoryid
  ), sum_yearly_sales_by_category_2012 AS (
       SELECT DISTINCT name, sales, productcategoryid 
         FROM sum_yearly_sales_by_category
        WHERE date_part('year', orderdate) = 2012
  ), sum_yearly_sales_by_category_2013 AS (
       SELECT DISTINCT name, sales, productcategoryid 
         FROM sum_yearly_sales_by_category
        WHERE date_part('year', orderdate) = 2013
  )
SELECT sys_13.name, sys_13.sales, (sys_13.sales - sys_12.sales) / sys_13.sales AS yoy  
  FROM sum_yearly_sales_by_category_2013 AS sys_13
  JOIN sum_yearly_sales_by_category_2012 AS sys_12
    ON sys_13.productcategoryid = sys_12.productcategoryid
 ORDER BY sys_13.name;
 
/* 5. Найти сумму максимальную заказа за каждый день января 2013, используя таблицы:
    Sales.SalesOrderHeader
    Sales.SalesOrderDetail */
SELECT DISTINCT soh.orderdate, 
                MAX(sod.linetotal) OVER (
                    PARTITION BY soh.orderdate) as max_order
  FROM Sales.SalesOrderHeader AS soh
  JOIN Sales.SalesOrderDetail AS sod
    ON soh.salesorderid = sod.salesorderid
 WHERE date_part('year', orderdate) = 2013 
   AND date_part('month', soh.orderdate) = 01;
   
/* 6. Найти товар, который чаще всего продавался 
   в каждой из субкатегорий в январе 2013, используя таблицы:
    Sales.SalesOrderHeader
    Sales.SalesOrderDetail
    Production.Product
    Production.ProductSubcategory */
	
-- Using MAX() with PARTITION BY:
WITH product_sales_in_jan_2013 AS (
       SELECT p.name AS product_name, 
              COUNT(*) OVER (
                  PARTITION BY sod.productid) AS count_sales, 
              ps.productsubcategoryid, 
              ps.name AS subcategory_name
         FROM Sales.SalesOrderHeader AS soh
         JOIN Sales.SalesOrderDetail AS sod
           ON soh.salesorderid = sod.salesorderid
         JOIN Production.Product AS p
           ON sod.productid = p.productid
         JOIN Production.ProductSubcategory AS ps
           ON p.productsubcategoryid = ps.productsubcategoryid
        WHERE date_part('year', soh.orderdate) = 2013 
          AND date_part('month', soh.orderdate) = 01
  ), max_quantity AS (
       SELECT subcategory_name, product_name, count_sales, 
              MAX(count_sales) OVER (
                  PARTITION BY productsubcategoryid) AS max_value 
         FROM product_sales_in_jan_2013)
SELECT DISTINCT subcategory_name, product_name AS most_frequent
  FROM max_quantity
 WHERE count_sales = max_value;

-- Using MAX() with GROUP BY:
WITH product_sales_in_jan_2013 AS (
       SELECT p.name AS product_name,  
              COUNT(*) OVER (
                  PARTITION BY sod.productid) AS count_sales, 
              ps.productsubcategoryid, 
              ps.name AS subcategory_name
         FROM Sales.SalesOrderHeader AS soh
         JOIN Sales.SalesOrderDetail AS sod
           ON soh.salesorderid = sod.salesorderid
         JOIN Production.Product AS p
           ON sod.productid = p.productid
         JOIN Production.ProductSubcategory AS ps
           ON p.productsubcategoryid = ps.productsubcategoryid
        WHERE date_part('year', soh.orderdate) = 2013 
          AND date_part('month', soh.orderdate) = 01
  ), max_quantity AS (
       SELECT MAX(count_sales) AS max_value, productsubcategoryid
         FROM product_sales_in_jan_2013
        GROUP BY productsubcategoryid)
SELECT DISTINCT subcategory_name, product_name as most_frequent
  FROM max_quantity AS mq
  JOIN product_sales_in_jan_2013 USING (productsubcategoryid)
 WHERE count_sales = max_value;
