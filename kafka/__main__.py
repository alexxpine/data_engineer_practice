from producer_raw_etnodim import retrieve_html
from producer_consumer_parse_dresses import get_title_from_html
from consumer_save_data import save_data

if __name__ == '__main__':
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',
        'Pragma': 'no-cache'
    }

    retrieve_html()
    get_title_from_html()
    save_data()
