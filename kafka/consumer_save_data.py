import json
import csv
from kafka import KafkaConsumer


def save_data():
    parsed_topic_name = 'parsed_etnodim'
    file_name = 'titles.csv'

    consumer = KafkaConsumer(parsed_topic_name, auto_offset_reset='earliest',
                             bootstrap_servers=['localhost:9092'], api_version=(0, 10), consumer_timeout_ms=1000)
    for msg in consumer:
        record = json.loads(msg.value)
        title = record['title']
        with open(file_name, 'a', encoding='UTF8') as file:
            writer = csv.writer(file)
            writer.writerow([title])

    if consumer is not None:
        consumer.close()
