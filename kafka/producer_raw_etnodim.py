from bs4 import BeautifulSoup
from kafka import KafkaProducer
import requests


def get_dresses_descriptions():
    dresses = []
    url = 'https://etnodim.com/en/women/dress/'
    print('Accessing list')

    try:
        r = requests.get(url)
        if r.status_code == 200:
            html = r.text
            soup = BeautifulSoup(html, 'lxml')
            links = soup.select('.bx_catalog_item_title a')
            for link in links:
                link = 'https://etnodim.com' + link['href']
                dress = fetch_raw(link)
                dresses.append(dress)
    except Exception as ex:
        print('Exception in get_dresses_descriptions')
        print(str(ex))
    finally:
        return dresses


def fetch_raw(etnodim_url):
    html = None
    print('Processing..{}'.format(etnodim_url))
    try:
        r = requests.get(etnodim_url)
        if r.status_code == 200:
            html = r.text
    except Exception as ex:
        print('Exception while accessing raw html')
        print(str(ex))
    finally:
        return html.strip()


def publish_message(producer_instance, topic_name, key, value):
    try:
        key_bytes = bytes(key, encoding='utf-8')
        value_bytes = bytes(value, encoding='utf-8')
        producer_instance.send(topic_name, key=key_bytes, value=value_bytes)
        producer_instance.flush()
        print('Message published successfully.')
    except Exception as ex:
        print('Exception in publishing message')
        print(str(ex))


def connect_kafka_producer():
    _producer = None
    try:
        _producer = KafkaProducer(bootstrap_servers=['localhost:9092'], api_version=(0, 10))
    except Exception as ex:
        print('Exception while connecting Kafka')
        print(str(ex))
    finally:
        return _producer


def retrieve_html():
    all_dresses = get_dresses_descriptions()
    if all_dresses:
        kafka_producer = connect_kafka_producer()
        for dress in all_dresses:
            publish_message(kafka_producer, 'raw_etnodim', 'raw', dress.strip())
        if kafka_producer is not None:
            kafka_producer.close()
