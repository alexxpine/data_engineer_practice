/* 1. Задача: Зная электронную почту сотрудника 
   нужно получить его и имя и фамилию, а также его возраст. 
   Используйте таблицы person.emailaddress, person.person, humanresources.employee 
   Параметры:
   <inp> – character varying(100).
   Возврат функции:
   <return> – varchar.
   Пример вывода: “<firstname> <lastname> - <age>” */

CREATE OR REPLACE FUNCTION sosnovskaya.get_employee_data_task1(
    IN email varchar(100), 
    OUT employee_data varchar
) AS 
$$
    SELECT p.firstname || ' ' || p.lastname || ' - ' || DATE_PART('year', CURRENT_DATE) - DATE_PART('year', e.birthdate) as employee_data
      FROM person.person AS p
     INNER JOIN humanresources.employee AS e
        ON p.businessentityid = e.businessentityid
     INNER JOIN person.emailaddress AS ea
        ON e.businessentityid = ea.businessentityid
     WHERE ea.emailaddress = email;
$$
LANGUAGE sql;

SELECT * FROM sosnovskaya.get_employee_data_task1('ken0@adventure-works.com');
SELECT * FROM sosnovskaya.get_employee_data_task1(''); --null
SELECT * FROM sosnovskaya.get_employee_data_task1(3); --type error

DROP FUNCTION IF EXISTS sosnovskaya.get_employee_data_task1(varchar);